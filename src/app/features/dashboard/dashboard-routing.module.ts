import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {IndexComponent} from './containers/index/index.component';

const routes: Routes = [
  {path: '', redirectTo: 'balance', pathMatch: 'full'},
  {
    path: '',
    component: IndexComponent,
    children: [
      // {path: '', redirectTo: 'balance', pathMatch: 'full'},
      {
        path: '',
        loadChildren: () => import('./components/balance/balance.module').then(m => m.BalanceModule),
      },
      {
        path: 'settings',
        loadChildren: () => import('./components/settings/settings.module').then(m => m.SettingsModule),
      },
      {
        path: 'history',
        loadChildren: () => import('./components/history/history.module').then(m => m.HistoryModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
