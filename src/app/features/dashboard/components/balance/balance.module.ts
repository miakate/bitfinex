import {BalanceComponent} from './features/balance/balance.component';
import {SharedModule} from '../../../../modules/shared/shared.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BalanceRoutingModule} from './balance-routing.module';

@NgModule({
  declarations: [
    BalanceComponent
  ],
  imports: [
    CommonModule,
    BalanceRoutingModule,
    SharedModule,
  ]
})
export class BalanceModule {
}
