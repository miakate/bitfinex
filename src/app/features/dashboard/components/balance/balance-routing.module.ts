import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {BalanceComponent} from './features/balance/balance.component';

const routes: Routes = [
  {path: '', component: BalanceComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BalanceRoutingModule {
}
