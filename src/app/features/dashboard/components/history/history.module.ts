import {HistoryComponent} from './features/history/history.component';
import {SharedModule} from '../../../../modules/shared/shared.module';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {HistoryRoutingModule} from './history-routing.module';

@NgModule({
  declarations: [
    HistoryComponent
  ],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    SharedModule,
  ]
})
export class HistoryModule {
}
