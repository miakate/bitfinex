import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

const sharedModules = [
  ReactiveFormsModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,

    ...sharedModules,
  ],
  exports: [
    FormsModule,

    ...sharedModules,
  ]
})
export class SharedModule {
}
